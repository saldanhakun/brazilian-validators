<?php


use Saldanhakun\BrazilianValidators\Constraint as Brazil;
use Symfony\Component\Validator\Constraints as Assert;
use Saldanhakun\BrazilianValidators\Validator\CnpjValidator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validation;

class CnpjValidatorTest extends TestCase
{

    // A valid CNPJ

    /**
     * An useless, symbolic CNPJ, that is nonetheless valid.
     */
    public const VALID_CNPJ_DIGITS = '12345678900188';
    /**
     * The same CNPJ, but fully punctuated as official standard
     */
    public const VALID_CNPJ_FULL = '12.345.678/9001-88';
    /**
     * The base digits for the same CNPJ
     */
    public const VALID_CNPJ_BASE = '123456789001';
    /**
     * The DV for the same CNPJ
     */
    public const VALID_CNPJ_DV = '88';

    // An invalid CNPJ (actually the same, with a wrong DV)

    /**
     * An useless, symbolic CNPJ, that is of course invalid.
     */
    public const INVALID_CNPJ_DIGITS = '12345678900122';
    /**
     * The same CNPJ, but fully punctuated as official standard
     */
    public const INVALID_CNPJ_FULL = '12.345.678/9001-22';
    /**
     * The base digits for the same CNPJ
     */
    public const INVALID_CNPJ_BASE = '123456789001';
    /**
     * The DV for the same CNPJ
     */
    public const INVALID_CNPJ_DV = '22';

    public function testCanNormalize()
    {
        // null value
        $this->assertTrue(CnpjValidator::canNormalize(null), "null must be normalizable");
        // digits only, for valid
        $this->assertTrue(CnpjValidator::canNormalize(self::VALID_CNPJ_DIGITS), self::VALID_CNPJ_DIGITS." must be normalizable");
        // full, for valid
        $this->assertTrue(CnpjValidator::canNormalize(self::VALID_CNPJ_FULL), self::VALID_CNPJ_FULL." must be normalizable");
        // digits only, for invalid
        $this->assertTrue(CnpjValidator::canNormalize(self::INVALID_CNPJ_DIGITS), self::INVALID_CNPJ_DIGITS." must be normalizable");
        // full, for invalid
        $this->assertTrue(CnpjValidator::canNormalize(self::INVALID_CNPJ_FULL), self::INVALID_CNPJ_FULL." must be normalizable");
        // non-zero leading base (interpreted as 2 leading zeroes not inputted)
        $this->assertTrue(CnpjValidator::canNormalize(self::VALID_CNPJ_BASE), self::VALID_CNPJ_BASE." must be normalizable");
        // any DV (interpreted as a too small value to be padded
        $this->assertFalse(CnpjValidator::canNormalize(self::INVALID_CNPJ_DV), self::INVALID_CNPJ_DV." must NOT be normalizable");
        // just a text (with 11 chars)
        $sometext = "some text!!";
        $this->assertFalse(CnpjValidator::canNormalize($sometext), $sometext." must NOT be normalizable");
        // just a number with less than MIN_LENGTH_TO_PAD digits
        $somenumber = random_int(0, (10 ** (CnpjValidator::MIN_LENGTH_TO_PAD-1)) - 1); // for '5', 10**4 = 10000 => 9999
        $this->assertFalse(CnpjValidator::canNormalize($somenumber), $somenumber." must NOT be normalizable");
        if (CnpjValidator::MIN_LENGTH_TO_PAD !== CnpjValidator::NUM_DIGITS) {
            // just a number with between MIN_LENGTH_TO_PAD and NUM_DIGITS digits
            $somenumber = random_int(10 ** (CnpjValidator::MIN_LENGTH_TO_PAD-1), (10 ** (CnpjValidator::NUM_DIGITS-1)) - 1);
            $this->assertTrue(CnpjValidator::canNormalize($somenumber), $somenumber . " must be normalizable");
        }
        // just a number with more than NUM_DIGITS digits
        $somenumber = random_int(10 ** (CnpjValidator::NUM_DIGITS-1), PHP_INT_MAX);
        $this->assertFalse(CnpjValidator::canNormalize($somenumber), $somenumber." must NOT be normalizable");
        // a CPF, valid or not, only digits (because of padding)
        $cnpj = '12345678909';
        $this->assertTrue(CnpjValidator::canNormalize($cnpj), $cnpj." must be normalizable");
        // a valid CPF, valid or not, full (because of padding)
        $cnpj = '123.456.789-09';
        $this->assertTrue(CnpjValidator::canNormalize($cnpj), $cnpj." must be normalizable");
    }

    public function testValidate()
    {
        $validator = Validation::createValidator();
        // Empty not required
        $violations = $validator->validate(null, [
            new Assert\Blank(),
            new Brazil\Cnpj(),
        ]);
        $this->assertEquals(0, $violations->count(), "null must be valid when not required");

        // Empty not required 2
        $violations = $validator->validate('', [
            new Assert\Blank(),
            new Brazil\Cnpj(),
        ]);
        $this->assertEquals(0, $violations->count(), "empty string must be valid when not required");

        // Empty but required
        $giveaway = 'because of NotBlank';
        $violations = $validator->validate(null, [
            new Assert\NotBlank(['message' => $giveaway]),
            new Brazil\Cnpj(),
        ]);
        $this->assertEquals(1, $violations->count(), "null must be invalid when required");
        $this->assertEquals($giveaway, $violations->get(0)->getMessage(), "null must be invalid when required");

        // Valid CNPJ (digits)
        $data = self::VALID_CNPJ_DIGITS;
        $violations = $validator->validate($data, [
            new Brazil\Cnpj(),
        ]);
        $this->assertEquals(0, $violations->count(), $data." must be valid");

        // Valid CNPJ (full)
        $data = self::VALID_CNPJ_FULL;
        $violations = $validator->validate($data, [
            new Brazil\Cnpj(),
        ]);
        $this->assertEquals(0, $violations->count(), $data." must be valid");

        // Invalid CNPJ (digits)
        $data = self::INVALID_CNPJ_DIGITS;
        $giveaway = 'because of DV';
        $violations = $validator->validate($data, [
            new Brazil\Cnpj(['dv_message' => $giveaway, 'hint_dv' => 'no']),
        ]);
        $this->assertEquals(1, $violations->count(), $data." must be invalid");
        $this->assertEquals($giveaway, $violations->get(0)->getMessage(), $data." must be invalid");

        // Invalid CNPJ (full)
        $data = self::INVALID_CNPJ_FULL;
        $giveaway = 'because of DV';
        $violations = $validator->validate($data, [
            new Brazil\Cnpj(['dv_message' => $giveaway, 'hint_dv' => 'no']),
        ]);
        $this->assertEquals(1, $violations->count(), $data." must be invalid");
        $this->assertEquals($giveaway, $violations->get(0)->getMessage(), $data." must be invalid");

        // Phone number with parenthesis (invalid char)
        $data = '55 (031) 98877-6655';
        $giveaway = 'because of invalid chars';
        $violations = $validator->validate($data, [
            new Brazil\Cnpj(['message' => $giveaway]),
        ]);
        $this->assertEquals(1, $violations->count(), $data." must be invalid");
        $this->assertEquals($giveaway, $violations->get(0)->getMessage(), $data." must be invalid");

        // Phone number with just digits (invalid DV)
        $data = '55 031 98877-6655';
        $giveaway = 'because of DV';
        $violations = $validator->validate($data, [
            new Brazil\Cnpj(['dv_message' => $giveaway, 'hint_dv' => 'no']),
        ]);
        $this->assertEquals(1, $violations->count(), $data." must be invalid");
        $this->assertEquals($giveaway, $violations->get(0)->getMessage(), $data." must be invalid");

        // Random sequence with NUM_DIGITS + 1 chars
        $data = random_int(10 ** CnpjValidator::NUM_DIGITS, PHP_INT_MAX);
        $giveaway = 'because of length';
        $violations = $validator->validate($data, [
            new Brazil\Cnpj(['length_message' => $giveaway]),
        ]);
        $this->assertEquals(1, $violations->count(), $data." must be invalid");
        $this->assertEquals($giveaway, $violations->get(0)->getMessage(), $data." must be invalid");

        // Caotic string with actual valid digits
        $data = '123 or 456 and 789 with 0+0 maybe 188';
        $giveaway = 'invalid chars';
        $violations = $validator->validate($data, [
            new Brazil\Cnpj(['message' => $giveaway]),
        ]);
        $this->assertEquals(1, $violations->count(), $data." must be invalid");
        $this->assertEquals($giveaway, $violations->get(0)->getMessage(), $data." must be invalid");

        // Same caotic string, but typed with only digits
        $data = preg_replace('/[^0-9]/', '', $data);
        $violations = $validator->validate($data, [
            new Brazil\Cnpj(),
        ]);
        $this->assertEquals(0, $violations->count(), $data." must be valid");

        // Valid CNPJ when padding...
        $data = str_pad((int)'00000000000191', CnpjValidator::MIN_LENGTH_TO_PAD, '0', STR_PAD_LEFT);
        $violations = $validator->validate($data, [
            new Brazil\Cnpj(),
        ]);
        $this->assertEquals(0, $violations->count(), $data." must be valid");

        // ... but invalid without padding
        $giveaway = 'not padded';
        $violations = $validator->validate($data, [
            new Brazil\Cnpj(['length_message' => $giveaway, 'pad_left' => false]),
        ]);
        $this->assertEquals(1, $violations->count(), $data." must be invalid without padding");
        $this->assertEquals($giveaway, $violations->get(0)->getMessage(), $data." must be invalid without padding");
    }

    public function testNormalize()
    {
        // empty request 1
        $this->assertNull(CnpjValidator::normalize(null), "Empty must normalize to null");
        // empty request 2
        $this->assertNull(CnpjValidator::normalize(''), "Empty must normalize to null");
        // unnormalizable request
        $this->assertNull(CnpjValidator::normalize(self::VALID_CNPJ_DV), self::VALID_CNPJ_DV." must not normalize");
        // unnormalizable request 2
        $sometext = 'some text!!';
        $this->assertNull(CnpjValidator::normalize($sometext), $sometext." must not normalize");
        // Valid normalization (digits)
        $this->assertEquals(self::VALID_CNPJ_FULL, CnpjValidator::normalize(self::VALID_CNPJ_DIGITS), self::VALID_CNPJ_DIGITS." must normalize to full form");
        // Valid normalization (full)
        $this->assertEquals(self::VALID_CNPJ_FULL, CnpjValidator::normalize(self::VALID_CNPJ_FULL), self::VALID_CNPJ_FULL." must normalize to full form");
        // Invalid normalization (digits)
        $this->assertEquals(self::INVALID_CNPJ_FULL, CnpjValidator::normalize(self::INVALID_CNPJ_DIGITS), self::INVALID_CNPJ_DIGITS." must normalize to full form");
        // Invalid normalization (full)
        $this->assertEquals(self::INVALID_CNPJ_FULL, CnpjValidator::normalize(self::INVALID_CNPJ_FULL), self::INVALID_CNPJ_FULL." must normalize to full form");
        // Valid normalization to digits (digits)
        $this->assertEquals(self::VALID_CNPJ_DIGITS, CnpjValidator::normalize(self::VALID_CNPJ_DIGITS, CnpjValidator::NORM_DIGITS), self::VALID_CNPJ_DIGITS." must normalize to digits form");
        // Valid normalization to digits (full)
        $this->assertEquals(self::VALID_CNPJ_DIGITS, CnpjValidator::normalize(self::VALID_CNPJ_FULL, CnpjValidator::NORM_DIGITS), self::VALID_CNPJ_FULL." must normalize to digits form");
        // Invalid normalization to digits (digits)
        $this->assertEquals(self::INVALID_CNPJ_DIGITS, CnpjValidator::normalize(self::INVALID_CNPJ_DIGITS, CnpjValidator::NORM_DIGITS), self::INVALID_CNPJ_DIGITS." must normalize to digits form");
        // Invalid normalization to digits (full)
        $this->assertEquals(self::INVALID_CNPJ_DIGITS, CnpjValidator::normalize(self::INVALID_CNPJ_FULL, CnpjValidator::NORM_DIGITS), self::INVALID_CNPJ_FULL." must normalize to digits form");
        // Valid caotic invalid paddable validation
        $sometext = 'CPF: 123456-789-10';
        $this->assertEquals('00.012.345/6789-10', CnpjValidator::normalize($sometext, CnpjValidator::NORM_FULL), $sometext." must pad and normalize to full form");
        // Valid hybrid validation
        $sometext = substr(self::VALID_CNPJ_BASE, 3) . '--' . self::VALID_CNPJ_DV;
        $this->assertEquals(str_replace('12.3','00.0',self::VALID_CNPJ_FULL), CnpjValidator::normalize($sometext, CnpjValidator::NORM_FULL), $sometext." must pad and normalize to full form");
        // Telephone normalization
        $sometext = '55 (031) 98877-6655';
        $this->assertEquals('55.031.988/7766-55', CnpjValidator::normalize($sometext, CnpjValidator::NORM_FULL), $sometext." must become a CNPJ");
    }

    public function testCalculateDv()
    {
        // empty request
        $this->assertEquals(CnpjValidator::INVALID_DV, CnpjValidator::calculateDv(''), "Empty must not calculate");
        // unnormalizable request
        $this->assertEquals(CnpjValidator::INVALID_DV, CnpjValidator::calculateDv(self::VALID_CNPJ_DV), self::VALID_CNPJ_DV." must not calculate");
        // unnormalizable request 2
        $sometext = 'some text!!';
        $this->assertEquals(CnpjValidator::INVALID_DV, CnpjValidator::calculateDv($sometext), $sometext." must not calculate");
        // Valid calculation (digits)
        $this->assertEquals(self::VALID_CNPJ_DV, CnpjValidator::calculateDv(self::VALID_CNPJ_DIGITS), self::VALID_CNPJ_DIGITS." must calculate and match");
        // Valid calculation (full)
        $this->assertEquals(self::VALID_CNPJ_DV, CnpjValidator::calculateDv(self::VALID_CNPJ_FULL), self::VALID_CNPJ_FULL." must calculate and match");
        // Wrong DV spotting (digits)
        $dv = CnpjValidator::calculateDv(self::INVALID_CNPJ_DIGITS);
        $this->assertNotEquals(CnpjValidator::INVALID_DV, $dv, self::INVALID_CNPJ_DIGITS." must calculate and not match");
        $this->assertNotEquals(self::INVALID_CNPJ_DV, $dv, self::INVALID_CNPJ_DIGITS." must calculate and not match");
        // Wrong DV spotting (full)
        $dv = CnpjValidator::calculateDv(self::INVALID_CNPJ_FULL);
        $this->assertNotEquals(CnpjValidator::INVALID_DV, $dv, self::INVALID_CNPJ_DIGITS." must calculate and not match");
        $this->assertNotEquals(self::INVALID_CNPJ_DV, $dv, self::INVALID_CNPJ_FULL." must calculate and not match");
        // Wrong DV not tampering
        $this->assertEquals(self::VALID_CNPJ_DV, CnpjValidator::calculateDv(self::INVALID_CNPJ_DIGITS), self::INVALID_CNPJ_DIGITS." must calculate and match with right DV");
        // CPF calculating (digits)
        $cnpj = '12345678909';
        $this->assertNotEquals(CnpjValidator::INVALID_DV, CnpjValidator::calculateDv($cnpj), $cnpj." must calculate");
        // CPF calculating (full)
        $cnpj = '123.456.789-09';
        $this->assertNotEquals(CnpjValidator::INVALID_DV, CnpjValidator::calculateDv($cnpj), $cnpj." must calculate");
    }
}
