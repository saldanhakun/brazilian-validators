<?php


use Saldanhakun\BrazilianValidators\Constraint as Brazil;
use Symfony\Component\Validator\Constraints as Assert;
use Saldanhakun\BrazilianValidators\Validator\CpfValidator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validation;

class CpfValidatorTest extends TestCase
{

    // A valid CPF

    /**
     * An useless, symbolic CPF, that is nonetheless valid.
     */
    public const VALID_CPF_DIGITS = '12345678909';
    /**
     * The same CPF, but fully punctuated as official standard
     */
    public const VALID_CPF_FULL = '123.456.789-09';
    /**
     * The base digits for the same CPF
     */
    public const VALID_CPF_BASE = '123456789';
    /**
     * The DV for the same CPF
     */
    public const VALID_CPF_DV = '09';

    // An invalid CPF (actually the same, with a wrong DV)

    /**
     * An useless, symbolic CPF, that is of course invalid.
     */
    public const INVALID_CPF_DIGITS = '12345678900';
    /**
     * The same CPF, but fully punctuated as official standard
     */
    public const INVALID_CPF_FULL = '123.456.789-00';
    /**
     * The base digits for the same CPF
     */
    public const INVALID_CPF_BASE = '123456789';
    /**
     * The DV for the same CPF
     */
    public const INVALID_CPF_DV = '00';

    public function testCanNormalize()
    {
        // null value
        $this->assertTrue(CpfValidator::canNormalize(null), "null must be normalizable");
        // digits only, for valid
        $this->assertTrue(CpfValidator::canNormalize(self::VALID_CPF_DIGITS), self::VALID_CPF_DIGITS." must be normalizable");
        // full, for valid
        $this->assertTrue(CpfValidator::canNormalize(self::VALID_CPF_FULL), self::VALID_CPF_FULL." must be normalizable");
        // digits only, for invalid
        $this->assertTrue(CpfValidator::canNormalize(self::INVALID_CPF_DIGITS), self::INVALID_CPF_DIGITS." must be normalizable");
        // full, for invalid
        $this->assertTrue(CpfValidator::canNormalize(self::INVALID_CPF_FULL), self::INVALID_CPF_FULL." must be normalizable");
        // non-zero leading base (interpreted as 2 leading zeroes not inputted)
        $this->assertTrue(CpfValidator::canNormalize(self::VALID_CPF_BASE), self::VALID_CPF_BASE." must be normalizable");
        // any DV (interpreted as a too small value to be padded
        $this->assertFalse(CpfValidator::canNormalize(self::INVALID_CPF_DV), self::INVALID_CPF_DV." must NOT be normalizable");
        // just a text (with 11 chars)
        $sometext = "some text!!";
        $this->assertFalse(CpfValidator::canNormalize($sometext), $sometext." must NOT be normalizable");
        // just a number with less than MIN_LENGTH_TO_PAD digits
        $somenumber = random_int(0, (10 ** (CpfValidator::MIN_LENGTH_TO_PAD-1)) - 1); // for '5', 10**4 = 10000 => 9999
        $this->assertFalse(CpfValidator::canNormalize($somenumber), $somenumber." must NOT be normalizable");
        if (CpfValidator::MIN_LENGTH_TO_PAD !== CpfValidator::NUM_DIGITS) {
            // just a number with between MIN_LENGTH_TO_PAD and NUM_DIGITS digits
            $somenumber = random_int(10 ** (CpfValidator::MIN_LENGTH_TO_PAD-1), (10 ** (CpfValidator::NUM_DIGITS-1)) - 1);
            $this->assertTrue(CpfValidator::canNormalize($somenumber), $somenumber . " must be normalizable");
        }
        // just a number with more than NUM_DIGITS digits
        $somenumber = random_int(10 ** (CpfValidator::NUM_DIGITS-1), PHP_INT_MAX);
        $this->assertFalse(CpfValidator::canNormalize($somenumber), $somenumber." must NOT be normalizable");
        // a valid CNPJ, only digits
        $cnpj = '00000000000191';
        $this->assertFalse(CpfValidator::canNormalize($cnpj), $cnpj." must NOT be normalizable");
        // a valid CNPJ, full
        $cnpj = '00.000.000/0001-91';
        $this->assertFalse(CpfValidator::canNormalize($cnpj), $cnpj." must NOT be normalizable");
    }

    public function testValidate()
    {
        $validator = Validation::createValidator();
        // Empty not required
        $violations = $validator->validate(null, [
            new Assert\Blank(),
            new Brazil\Cpf(),
        ]);
        $this->assertEquals(0, $violations->count(), "null must be valid when not required");

        // Empty not required 2
        $violations = $validator->validate('', [
            new Assert\Blank(),
            new Brazil\Cpf(),
        ]);
        $this->assertEquals(0, $violations->count(), "empty string must be valid when not required");

        // Empty but required
        $giveaway = 'because of NotBlank';
        $violations = $validator->validate(null, [
            new Assert\NotBlank(['message' => $giveaway]),
            new Brazil\Cpf(),
        ]);
        $this->assertEquals(1, $violations->count(), "null must be invalid when required");
        $this->assertEquals($giveaway, $violations->get(0)->getMessage(), "null must be invalid when required");

        // Valid CPF (digits)
        $data = self::VALID_CPF_DIGITS;
        $violations = $validator->validate($data, [
            new Brazil\Cpf(),
        ]);
        $this->assertEquals(0, $violations->count(), $data." must be valid");

        // Valid CPF (full)
        $data = self::VALID_CPF_FULL;
        $violations = $validator->validate($data, [
            new Brazil\Cpf(),
        ]);
        $this->assertEquals(0, $violations->count(), $data." must be valid");

        // Invalid CPF (digits)
        $data = self::INVALID_CPF_DIGITS;
        $giveaway = 'because of DV';
        $violations = $validator->validate($data, [
            new Brazil\Cpf(['dv_message' => $giveaway, 'hint_dv' => 'no']),
        ]);
        $this->assertEquals(1, $violations->count(), $data." must be invalid");
        $this->assertEquals($giveaway, $violations->get(0)->getMessage(), $data." must be invalid");

        // Invalid CPF (full)
        $data = self::INVALID_CPF_FULL;
        $giveaway = 'because of DV';
        $violations = $validator->validate($data, [
            new Brazil\Cpf(['dv_message' => $giveaway, 'hint_dv' => 'no']),
        ]);
        $this->assertEquals(1, $violations->count(), $data." must be invalid");
        $this->assertEquals($giveaway, $violations->get(0)->getMessage(), $data." must be invalid");

        // Phone number with parenthesis (invalid char)
        $data = '(31) 98877-6655';
        $giveaway = 'because of invalid chars';
        $violations = $validator->validate($data, [
            new Brazil\Cpf(['message' => $giveaway]),
        ]);
        $this->assertEquals(1, $violations->count(), $data." must be invalid");
        $this->assertEquals($giveaway, $violations->get(0)->getMessage(), $data." must be invalid");

        // Phone number with just digits (invalid DV)
        $data = '31 98877-6655';
        $giveaway = 'because of DV';
        $violations = $validator->validate($data, [
            new Brazil\Cpf(['dv_message' => $giveaway, 'hint_dv' => 'no']),
        ]);
        $this->assertEquals(1, $violations->count(), $data." must be invalid");
        $this->assertEquals($giveaway, $violations->get(0)->getMessage(), $data." must be invalid");

        // Phone number (extra digits) -- could also be a CNPJ, with same results
        $data = '55.31.98877-6655';
        $giveaway = 'because of length';
        $violations = $validator->validate($data, [
            new Brazil\Cpf(['length_message' => $giveaway]),
        ]);
        $this->assertEquals(1, $violations->count(), $data." must be invalid");
        $this->assertEquals($giveaway, $violations->get(0)->getMessage(), $data." must be invalid");

        // Caotic string with actual valid digits
        $data = '123 or 456 and 789 with 09';
        $giveaway = 'invalid chars';
        $violations = $validator->validate($data, [
            new Brazil\Cpf(['message' => $giveaway]),
        ]);
        $this->assertEquals(1, $violations->count(), $data." must be invalid");
        $this->assertEquals($giveaway, $violations->get(0)->getMessage(), $data." must be invalid");

        // Same caotic string, but typed with only digits
        $data = preg_replace('/[^0-9]/', '', $data);
        $violations = $validator->validate($data, [
            new Brazil\Cpf(),
        ]);
        $this->assertEquals(0, $violations->count(), $data." must be valid");

        // Valid CPF when padding...
        $data = (string)(int)'00100200397';
        $violations = $validator->validate($data, [
            new Brazil\Cpf(),
        ]);
        $this->assertEquals(0, $violations->count(), $data." must be valid");

        // ... but invalid without padding
        $giveaway = 'not padded';
        $violations = $validator->validate($data, [
            new Brazil\Cpf(['length_message' => $giveaway, 'pad_left' => false]),
        ]);
        $this->assertEquals(1, $violations->count(), $data." must be invalid without padding");
        $this->assertEquals($giveaway, $violations->get(0)->getMessage(), $data." must be invalid without padding");
    }

    public function testNormalize()
    {
        // empty request 1
        $this->assertNull(CpfValidator::normalize(null), "Empty must normalize to null");
        // empty request 2
        $this->assertNull(CpfValidator::normalize(''), "Empty must normalize to null");
        // unnormalizable request
        $this->assertNull(CpfValidator::normalize(self::VALID_CPF_DV), self::VALID_CPF_DV." must not normalize");
        // unnormalizable request 2
        $sometext = 'some text!!';
        $this->assertNull(CpfValidator::normalize($sometext), $sometext." must not normalize");
        // Valid normalization (digits)
        $this->assertEquals(self::VALID_CPF_FULL, CpfValidator::normalize(self::VALID_CPF_DIGITS), self::VALID_CPF_DIGITS." must normalize to full form");
        // Valid normalization (full)
        $this->assertEquals(self::VALID_CPF_FULL, CpfValidator::normalize(self::VALID_CPF_FULL), self::VALID_CPF_FULL." must normalize to full form");
        // Invalid normalization (digits)
        $this->assertEquals(self::INVALID_CPF_FULL, CpfValidator::normalize(self::INVALID_CPF_DIGITS), self::INVALID_CPF_DIGITS." must normalize to full form");
        // Invalid normalization (full)
        $this->assertEquals(self::INVALID_CPF_FULL, CpfValidator::normalize(self::INVALID_CPF_FULL), self::INVALID_CPF_FULL." must normalize to full form");
        // Valid normalization to digits (digits)
        $this->assertEquals(self::VALID_CPF_DIGITS, CpfValidator::normalize(self::VALID_CPF_DIGITS, CpfValidator::NORM_DIGITS), self::VALID_CPF_DIGITS." must normalize to digits form");
        // Valid normalization to digits (full)
        $this->assertEquals(self::VALID_CPF_DIGITS, CpfValidator::normalize(self::VALID_CPF_FULL, CpfValidator::NORM_DIGITS), self::VALID_CPF_FULL." must normalize to digits form");
        // Invalid normalization to digits (digits)
        $this->assertEquals(self::INVALID_CPF_DIGITS, CpfValidator::normalize(self::INVALID_CPF_DIGITS, CpfValidator::NORM_DIGITS), self::INVALID_CPF_DIGITS." must normalize to digits form");
        // Invalid normalization to digits (full)
        $this->assertEquals(self::INVALID_CPF_DIGITS, CpfValidator::normalize(self::INVALID_CPF_FULL, CpfValidator::NORM_DIGITS), self::INVALID_CPF_FULL." must normalize to digits form");
        // Valid caotic invalid paddable validation
        $sometext = 'CPF: 123456-78';
        $this->assertEquals('000.123.456-78', CpfValidator::normalize($sometext, CpfValidator::NORM_FULL), $sometext." must pad and normalize to full form");
        // Valid hybrid validation
        $sometext = substr(self::VALID_CPF_BASE, 3) . '--' . self::VALID_CPF_DV;
        $this->assertEquals(str_replace('123','000',self::VALID_CPF_FULL), CpfValidator::normalize($sometext, CpfValidator::NORM_FULL), $sometext." must pad and normalize to full form");
        // Telephone normalization
        $sometext = '(31) 98877-6655';
        $this->assertEquals('319.887.766-55', CpfValidator::normalize($sometext, CpfValidator::NORM_FULL), $sometext." must become a CPF");
    }

    public function testCalculateDv()
    {
        // empty request
        $this->assertEquals(CpfValidator::INVALID_DV, CpfValidator::calculateDv(''), "Empty must not calculate");
        // unnormalizable request
        $this->assertEquals(CpfValidator::INVALID_DV, CpfValidator::calculateDv(self::VALID_CPF_DV), self::VALID_CPF_DV." must not calculate");
        // unnormalizable request 2
        $sometext = 'some text!!';
        $this->assertEquals(CpfValidator::INVALID_DV, CpfValidator::calculateDv($sometext), $sometext." must not calculate");
        // Valid calculation (digits)
        $this->assertEquals(self::VALID_CPF_DV, CpfValidator::calculateDv(self::VALID_CPF_DIGITS), self::VALID_CPF_DIGITS." must calculate and match");
        // Valid calculation (full)
        $this->assertEquals(self::VALID_CPF_DV, CpfValidator::calculateDv(self::VALID_CPF_FULL), self::VALID_CPF_FULL." must calculate and match");
        // Wrong DV spotting (digits)
        $dv = CpfValidator::calculateDv(self::INVALID_CPF_DIGITS);
        $this->assertNotEquals(CpfValidator::INVALID_DV, $dv, self::INVALID_CPF_DIGITS." must calculate and not match");
        $this->assertNotEquals(self::INVALID_CPF_DV, $dv, self::INVALID_CPF_DIGITS." must calculate and not match");
        // Wrong DV spotting (full)
        $dv = CpfValidator::calculateDv(self::INVALID_CPF_FULL);
        $this->assertNotEquals(CpfValidator::INVALID_DV, $dv, self::INVALID_CPF_DIGITS." must calculate and not match");
        $this->assertNotEquals(self::INVALID_CPF_DV, $dv, self::INVALID_CPF_FULL." must calculate and not match");
        // Wrong DV not tampering
        $this->assertEquals(self::VALID_CPF_DV, CpfValidator::calculateDv(self::INVALID_CPF_DIGITS), self::INVALID_CPF_DIGITS." must calculate and match with right DV");
        // CNPJ calculating (digits)
        $cnpj = '00000000000191';
        $this->assertEquals(CpfValidator::INVALID_DV, CpfValidator::calculateDv($cnpj), $cnpj." must not calculate");
        // CNPJ calculating (full)
        $cnpj = '00.000.000/0001-91';
        $this->assertEquals(CpfValidator::INVALID_DV, CpfValidator::calculateDv($cnpj), $cnpj." must not calculate");
    }
}
