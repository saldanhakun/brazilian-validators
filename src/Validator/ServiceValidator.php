<?php

namespace Saldanhakun\BrazilianValidators\Validator;

use Saldanhakun\BrazilianValidators\Constraint\AbstractPhone;
use Saldanhakun\BrazilianValidators\Constraint\Service;

/**
 *
 * @author Marcelo Saldanha <saldanha@nautilos.com.br>
 * @license GPL-3.0-or-later
 */
class ServiceValidator extends PhoneValidator
{

    public const VALID_SERVICES = ['100', '128', '151', '152', '153', '156', '180', '181', '185', '188', '190', '191', '192', '193', '194', '197', '198', '199', '102', '130', '134'];
    
    /**
     * @param string $value
     * @param string $digits
     * @param Service $constraint
     */
    protected function validateServiceNumber(string $value, string $digits, AbstractPhone $constraint): void
    {
        $length = strlen($digits);
        if ($length === 3) {
            if (in_array($digits, self::VALID_SERVICES)) {
                return; // Valid
            }
        }
        if ($length === 4) {
            if (preg_match('/^(105)/', $digits)) {
                return; // Valid
            }
        }
        if ($length === 5) {
            if (preg_match('/^(103|106)/', $digits)) {
                return; // Valid
            }
        }
        if ($length === 8) {
            if (preg_match('/^(300|400)/', $digits)) {
                return; // Valid
            }
        }
        // Everything else is rubbish
        $this->context->buildViolation($constraint->message)
            ->setParameter('{{ value }}', $value)
            ->addViolation();
    }

    protected function isLandlineAllowed(): bool
    {
        return false;
    }

    protected function isMobileAllowed(): bool
    {
        return false;
    }

    /**
     * Normalizes a local number according to its nature
     * @param string|null $country
     * @param string|null $area
     * @param string $digits
     * @return string
     */
    protected static function normalizeLocal(?string $country, ?string $area, string $digits): string
    {
        $length = strlen($digits);
        if ($length === 8) {
            return sprintf('%s-%s', substr($digits, 0, 4), substr($digits, 4));
        }
        if ($length === 5) {
            return sprintf('%s %s', substr($digits, 0, 3), substr($digits, 3));
        }
        return parent::normalizeLocal($country, $area, $digits);
    }

}
