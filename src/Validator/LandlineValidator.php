<?php

namespace Saldanhakun\BrazilianValidators\Validator;

/**
 *
 * @author Marcelo Saldanha <saldanha@nautilos.com.br>
 * @license GPL-3.0-or-later
 */
class LandlineValidator extends PhoneValidator
{

    protected function isMobileAllowed(): bool
    {
        return false;
    }
}
