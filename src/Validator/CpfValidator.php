<?php

namespace Saldanhakun\BrazilianValidators\Validator;

use Saldanhakun\BrazilianValidators\Constraint\Cpf;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\LogicException;

/**
 *
 * @author Marcelo Saldanha <saldanha@nautilos.com.br>
 * @license GPL-3.0-or-later
 */
class CpfValidator extends ConstraintValidator
{

    /**
     * Normalization strategy: all digits, with all punctuation
     */
    public const NORM_FULL = 'full';
    /**
     * Normalization strategy: only digits, with no punctuation
     */
    public const NORM_DIGITS = 'digits';
    /**
     * Normalization strategy: keep input just as received
     */
    public const NORM_NONE = 'none';

    /**
     * Length of ORM columns for storing using NORM_FULL strategy
     */
    public const ORM_COLUMN_FULL_LENGTH = 14;
    /**
     * Length of ORM columns for storing using NORM_DIGITS strategy
     */
    public const ORM_COLUMN_DIGITS_LENGTH = 11;
    /**
     * Length of ORM columns for storing using NORM_NONE strategy
     */
    public const ORM_COLUMN_NONE_LENGTH = self::ORM_LENGTH;

    /**
     * A safe column length for storing CPFs, regardless of the normalization strategy.
     * Actually, it also allows to store CNPJs, in case a mixed usage is in place.
     */
    public const ORM_LENGTH = CnpjValidator::ORM_LENGTH;

    /**
     * Some minimal threshold to allow interpretation of values as CPF with non inputted leading zeroes
     */
    public const MIN_LENGTH_TO_PAD = 5;
    /**
     * Length of fully punctuated CPFs
     */
    public const FULL_LENGTH = 14;
    /**
     * Number of digits in a valid CPF
     */
    public const NUM_DIGITS = 11;
    /**
     * Uncalculated DV
     */
    public const INVALID_DV = '--';
    /**
     * Pattern for Regex validation (only digits)
     */
    public const REGEX_DIGITS = '/[0-9]{'.self::MIN_LENGTH_TO_PAD.','.self::NUM_DIGITS.'}/';
    /**
     * Pattern for Regex validation (punctuated)
     */
    public const REGEX_FULL = '/[0-9]{3}.[0-9]{3}.[0-9]{3}-[0-9]{2}/';

    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint): void
    {
        /* @var Cpf $constraint */

        if (null === $value || '' === trim($value)) {
            /* Just ignore empty values, as usual */
            return;
        }

        if (preg_replace('/[-0-9. ]/', '', $value) !== '') {
            // CPF only allows for digits and a few masking characters. Refuses anything else.
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $value)
                ->addViolation();
        } else {
            // Some people type the punctuation, other people don't. We must ignore them and look at the digits only.
            $number = preg_replace('/[^0-9]/', '', $value);
            if ($constraint->pad_left && strlen($number) >= self::MIN_LENGTH_TO_PAD) {
                // some people don't type in leading zeroes. Not that common, but can be handled by padding zero.
                // The minimum length is an ad hoc threshold to ignore inputs like '1234' and sorts.
                $number = str_pad($number, self::NUM_DIGITS, '0', STR_PAD_LEFT);
            }
            if (strlen($number) !== self::NUM_DIGITS) {
                // Valid CPF always have 11 digits
                $this->context->buildViolation($constraint->length_message)
                    ->setParameter('{{ value }}', $value)
                    ->addViolation();
            } else {
                $dv = static::calculateDv($number);
                $input_dv = substr($number, self::NUM_DIGITS-2, 2);
                // Validation is actually done by comparing the last 2 digits with the expected DV calculated
                if ($dv !== $input_dv) {
                    $message = $constraint->dv_message;
                    if (in_array($constraint->hint_dv, ['yes', getenv('APP_ENV')], true)) {
                        $message = $constraint->dv_message_hinted;
                    }
                    $this->context->buildViolation($message)
                        ->setParameter('{{ value }}', $value)
                        ->setParameter('{{ dv }}', $dv)
                        ->setParameter('{{ input_dv }}', $input_dv)
                        ->addViolation();
                }
            }
        }
    }

    /**
     * Calculated the DV for the CPF, using a "mod11" approach.
     * This implementation is based on something I found years ago on iMasters
     * @see http://imasters.com.br/artigo/2410/javascript/algoritmo_do_cpf/
     * @param string $cpf
     * @return string two-digits DV
     */
    public static function calculateDv(string $cpf): string
    {
        if ($cpf === '' || !self::canNormalize($cpf)) {
            return self::INVALID_DV;
        }
        // normalizes the input, although it was probably cleaned before calling this method...
        $str = self::normalize($cpf, self::NORM_DIGITS);

        $a = array();
        $b = 0;
        $c = 11;
        for ($i = 0; $i < 11; $i++) {
            $a[$i] = $str[$i];
            if ($i < 9) $b += ($a[$i] * --$c);
        }
        $x = $b % 11;
        if ($x < 2) {
            $a[9] = 0;
        } else {
            $a[9] = 11 - $x;
        }
        $b = 0;
        $c = 11;
        for ($y = 0; $y < 10; $y++) {
            $b += ($a[$y] * $c--);
        }
        $x = $b % 11;
        if ($x < 2) {
            $a[10] = 0;
        } else {
            $a[10] = 11 - $x;
        }

        return $a[9] . $a[10];
    }

    /**
     * Tests whether a string seems like a CPF that could be normalized
     * Note that this does not handle validation at all.
     * @param string|null $cpf
     * @return bool
     */
    public static function canNormalize(?string $cpf): bool
    {
        if ($cpf === null) {
            // blank values are, theoretically, normalized.
            return true;
        }
        else {
            $digits = preg_replace('/[^0-9]/', '', $cpf);
            if (strlen($digits) < self::MIN_LENGTH_TO_PAD) {
                return false;
            }
            $number = str_pad($digits, self::NUM_DIGITS, '0', STR_PAD_LEFT);
            // If it has the required length of digits, should be a CPF
            return strlen($number) === self::NUM_DIGITS;
        }
    }

    /**
     * Normalize a CPF (valid or not) according to official standards (e.g. 999.999.999-99)
     * @param string|null $cpf
     * @param string $strategy
     * @return string
     */
    public static function normalize(?string $cpf, string $strategy = self::NORM_FULL): ?string
    {
        if ($cpf === null || !self::canNormalize($cpf)) {
            return null;
        }
        $number = str_pad(preg_replace('/[^0-9]/', '', $cpf), self::NUM_DIGITS, '0', STR_PAD_LEFT);

        if ($strategy === self::NORM_FULL) {
            $pretty = '';
            $p = 0;
            $q = 3;
            $pretty .= substr($number, $p, $q);
            $p += $q;
            $pretty .= '.' . substr($number, $p, $q);
            $p += $q;
            $pretty .= '.' . substr($number, $p, $q);
            $p += $q;
            $pretty .= '-' . substr($number, $p);

            return $pretty;
        } elseif ($strategy === self::NORM_DIGITS) {
            return $number;
        } elseif ($strategy === self::NORM_NONE) {
            return trim($cpf);
        } else {
            throw new LogicException("Invalid normalization strategy: $strategy");
        }
    }
}
