<?php

namespace Saldanhakun\BrazilianValidators\Validator;

use Saldanhakun\BrazilianValidators\Constraint\Cnpj;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\LogicException;

/**
 *
 * @author Marcelo Saldanha <saldanha@nautilos.com.br>
 * @license GPL-3.0-or-later
 */
class CnpjValidator extends ConstraintValidator
{

    /**
     * Normalization strategy: all digits, with all punctuation
     */
    public const NORM_FULL = 'full';
    /**
     * Normalization strategy: only digits, with no punctuation
     */
    public const NORM_DIGITS = 'digits';
    /**
     * Normalization strategy: keep input just as received
     */
    public const NORM_NONE = 'none';

    /**
     * Length of ORM columns for storing using NORM_FULL strategy
     */
    public const ORM_COLUMN_FULL_LENGTH = 18;
    /**
     * Length of ORM columns for storing using NORM_DIGITS strategy
     */
    public const ORM_COLUMN_DIGITS_LENGTH = 14;
    /**
     * Length of ORM columns for storing using NORM_NONE strategy
     */
    public const ORM_COLUMN_NONE_LENGTH = self::ORM_LENGTH;

    /**
     * A safe column length for storing CNPJs, regardless of the normalization strategy.
     * Actually, it also allows to store CPFs, in case a mixed usage is in place.
     */
    public const ORM_LENGTH = 20;

    /**
     * Some minimal threshold to allow interpretation of values as CNPJ with non inputted leading zeroes
     */
    public const MIN_LENGTH_TO_PAD = 10;
    /**
     * Length of fully punctuated CNPJs
     */
    public const FULL_LENGTH = 18;
    /**
     * Number of digits in a valid CNPJ
     */
    public const NUM_DIGITS = 14;
    /**
     * Uncalculated DV
     */
    public const INVALID_DV = '--';
    /**
     * Pattern for Regex validation (only digits)
     */
    public const REGEX_DIGITS = '/[0-9]{'.self::MIN_LENGTH_TO_PAD.','.self::NUM_DIGITS.'}/';
    /**
     * Pattern for Regex validation (punctuated)
     */
    public const REGEX_FULL = '@[0-9]{2}.[0-9]{3}.[0-9]{3}/[0-9]{4}-[0-9]{2}@';

    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint): void
    {
        /* @var Cnpj $constraint */

        if (null === $value || '' === trim($value)) {
            /* Just ignore empty values, as usual */
            return;
        }

        if (preg_replace('/[-0-9. \/]/', '', $value) !== '') {
            // CNPJ only allows for digits and a few masking characters. Refuses anything else.
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $value)
                ->addViolation();
        } else {
            // Some people type the punctuation, other people don't. We must ignore them and look at the digits only.
            $number = preg_replace('/[^0-9]/', '', $value);
            if ($constraint->pad_left && strlen($number) >= self::MIN_LENGTH_TO_PAD) {
                // some people don't type in leading zeroes. Not that common, but can be handled by padding zero.
                // The minimum digit minimum is an ad hoc threshold to ignore inputs like '1234' and sorts.
                $number = str_pad($number, self::NUM_DIGITS, '0', STR_PAD_LEFT);
            }
            if (strlen($number) !== self::NUM_DIGITS) {
                // Valid CNPJ always have 14 digits
                $this->context->buildViolation($constraint->length_message)
                    ->setParameter('{{ value }}', $value)
                    ->addViolation();
            } else {
                $dv = static::calculateDv($number);
                $input_dv = substr($number, self::NUM_DIGITS-2, 2);
                // Validation is actually done by comparing the last 2 digits with the expected DV calculated
                if ($dv !== $input_dv) {
                    $message = $constraint->dv_message;
                    if (in_array($constraint->hint_dv, ['yes', getenv('APP_ENV')], true)) {
                        $message = $constraint->dv_message_hinted;
                    }
                    $this->context->buildViolation($message)
                        ->setParameter('{{ value }}', $value)
                        ->setParameter('{{ dv }}', $dv)
                        ->setParameter('{{ input_dv }}', $input_dv)
                        ->addViolation();
                }
            }
        }
    }

    /**
     * Calculated the DV for the CNPJ, using a "mod11" approach.
     * This implementation is based on something I found years ago on iMasters
     * @see http://imasters.com.br/artigo/2410/javascript/algoritmo_do_Cnpj/
     * @param string $cnpj
     * @return string two-digits DV
     */
    public static function calculateDv(string $cnpj): string
    {
        if ($cnpj === '' || !self::canNormalize($cnpj)) {
            return self::INVALID_DV;
        }
        // normalizes the input, although it was probably cleaned before calling this method...
        $str = self::normalize($cnpj, self::NORM_DIGITS);

        $a = array();
        $b = 0;
        $c = array(6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2);
        for ($k = 0; $k < 12; $k++) {
            $a[$k] = $str[$k];
            $b += $a[$k] * $c[$k + 1];
        }
        $x = $b % 11;
        if ($x < 2) {
            $a[12] = 0;
        } else {
            $a[12] = 11 - $x;
        }
        $b = 0;
        for ($k = 0; $k < 13; $k++) {
            $b += $a[$k] * $c[$k];
        }
        $x = $b % 11;
        if ($x < 2) {
            $a[13] = 0;
        } else {
            $a[13] = 11 - $x;
        }

        return $a[12] . $a[13];
    }

    /**
     * Tests whether a string seems like a CNPJ that could be normalized.
     * Note that this does not handle validation at all.
     * @param string|null $cnpj
     * @return bool
     */
    public static function canNormalize(?string $cnpj): bool
    {
        if ($cnpj === null) {
            // blank values are, theoretically, normalized.
            return true;
        }
        else {
            $digits = preg_replace('/[^0-9]/', '', $cnpj);
            if (strlen($digits) < self::MIN_LENGTH_TO_PAD) {
                return false;
            }
            $number = str_pad($digits, self::NUM_DIGITS, '0', STR_PAD_LEFT);
            // If it has the required length of digits, should be a CNPJ
            return strlen($number) === self::NUM_DIGITS;
        }
    }

    /**
     * Normaliza um valor supostamente validado como um CNPJ oficial (99.999.999/9999-99)
     * @param string|null $cnpj
     * @param string $strategy
     * @return string
     */
    public static function normalize(?string $cnpj, string $strategy = self::NORM_FULL): ?string
    {
        if ($cnpj === null || !self::canNormalize($cnpj)) {
            return null;
        }
        $number = str_pad(preg_replace('/[^0-9]/', '', $cnpj), self::NUM_DIGITS, '0', STR_PAD_LEFT);

        if ($strategy === self::NORM_FULL) {
            $pretty = '';
            $p = 0;
            $q = 2;
            $pretty .= substr($number, $p, $q);
            $p += $q++;
            $pretty .= '.' . substr($number, $p, $q);
            $p += $q;
            $pretty .= '.' . substr($number, $p, $q);
            $p += $q++;
            $pretty .= '/' . substr($number, $p, $q);
            $p += $q;
            $pretty .= '-' . substr($number, $p);

            return $pretty;
        } elseif ($strategy === self::NORM_DIGITS) {
            return $number;
        } elseif ($strategy === self::NORM_NONE) {
            return trim($cnpj);
        } else {
            throw new LogicException("Invalid normalization strategy: $strategy");
        }
    }
}
