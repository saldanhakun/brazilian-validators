<?php

namespace Saldanhakun\BrazilianValidators\Validator;

use Saldanhakun\BrazilianValidators\Constraint\AbstractPhone;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Exception\LogicException;

/**
 *
 * @author Marcelo Saldanha <saldanha@nautilos.com.br>
 * @license GPL-3.0-or-later
 */
abstract class AbstractPhoneValidator extends ConstraintValidator
{

    public const NORM_INTERNATIONAL = 'full';
    public const NORM_INTERNATIONAL_DIGITS = 'digits';
    public const NORM_LOCAL = 'local';
    public const NORM_LOCAL_DIGITS = 'local-digits';
    public const NORM_NONE = 'none';

    public const ORM_LENGTH = 30;

    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint): void
    {
        /* @var AbstractPhone $constraint */

        $trimmed = trim($value);
        if (null === $value || '' === $trimmed) {
            /* Just ignore empty values, as usual */
            return;
        }

        if (preg_replace('/[-+0-9(). ]/', '', $value) !== '') {
            // Phones only allows for digits and a few masking characters. Refuses anything else.
            $this->context->buildViolation($constraint->invalid_char_message)
                ->setParameter('{{ value }}', $value)
                ->addViolation();
            return;
        }
        // The plus sign is a special case: if used, must be the first character, and can only appear once.
        $plus = strpos($trimmed, '+');
        if ($plus !== false) {
            if ($plus !== 0 || substr_count($value, '+') > 1) {
                $this->context->buildViolation($constraint->plus_message)
                    ->setParameter('{{ value }}', $value)
                    ->addViolation();
                return;
            }
            // Interprets the country code, and identified the local number
            list($country, $digits) = static::splitCountry($value, $this->context, $constraint);

            $this->validateInternationalPhone($value, $country, $digits, $constraint);
        }
        else {
            // No country present, interprets as a local number
            $digits = preg_replace('/[^0-9]/', '', $value);
            $this->validateLocalPhone($value, $digits, $constraint);
        }

    }

    /**
     * Validate a phone number with no country assigned
     * @param string $value
     * @param string $digits
     * @param AbstractPhone $constraint
     */
    abstract protected function validateLocalPhone(string $value, string $digits, AbstractPhone $constraint): void;

    /**
     * Validate a phone number with no country assigned
     * @param string $value
     * @param string $country
     * @param string $digits
     * @param AbstractPhone $constraint
     */
    abstract protected function validateInternationalPhone(string $value, string $country, string $digits, AbstractPhone $constraint): void;

    /**
     * @param string $value
     * @param string $ddd
     * @param string $number
     * @param AbstractPhone $constraint
     */
    protected function validateMobileNumber(string $value, string $ddd, string $number, AbstractPhone $constraint): void
    {
        // This validator doesn't allow these numbers
        $this->context->buildViolation($constraint->mobile_number_message)
            ->setParameter('{{ value }}', $value)
            ->addViolation();
    }

    /**
     * @param string $value
     * @param string $ddd
     * @param string $number
     * @param AbstractPhone $constraint
     */
    protected function validateLandlineNumber(string $value, string $ddd, string $number, AbstractPhone $constraint): void
    {
        // This validator doesn't allow these numbers
        $this->context->buildViolation($constraint->landline_number_message)
            ->setParameter('{{ value }}', $value)
            ->addViolation();
    }

    /**
     * @param string $value
     * @param string $digits
     * @param AbstractPhone $constraint
     */
    protected function validateServiceNumber(string $value, string $digits, AbstractPhone $constraint): void
    {
        // This validator doesn't allow these numbers
        $this->context->buildViolation($constraint->service_number_message)
            ->setParameter('{{ value }}', $value)
            ->addViolation();
    }

    /**
     * @param string $value
     * @param string $digits
     * @param AbstractPhone $constraint
     */
    protected function validateTollNumber(string $value, string $digits, AbstractPhone $constraint): void
    {
        // This validator doesn't allow these numbers
        $this->context->buildViolation($constraint->toll_number_message)
            ->setParameter('{{ value }}', $value)
            ->addViolation();
    }

    // refactor for signaling a country violation
    private static function failUnassigned(string $value, string $country, ?ExecutionContextInterface $context, ?AbstractPhone $constraint): array
    {
        if ($context && $constraint) {
            $context->buildViolation($constraint->unassigned_country_message)
                ->setParameter('{{ value }}', $value)
                ->setParameter('{{ country }}', $country)
                ->addViolation();
        }
        return [$country, null];
    }

    /**
     * Interprets the country code used, and returns it split from the local number.
     * @param string $value
     * @param ?ExecutionContextInterface $context
     * @param ?AbstractPhone $constraint
     * @return array
     */
    protected static function splitCountry(string $value, ?ExecutionContextInterface $context, ?AbstractPhone $constraint): array
    {
        /**
         * This is quite a complex task. Country Codes are assigned according to a international standard, and
         * can have from 1 to 3 digits in length. Wikipedia holds a useful table we used to detect the country.
         * @see https://en.wikipedia.org/wiki/List_of_country_calling_codes
         */
        $digits = preg_replace('/[^0-9]/', '', $value);
        $pos = 0;
        $country = $digits[$pos++];
        switch ($country[0]) {
            case '1':
                // North America and other territories (1-digit)
                break;
            case '2':
                $country .= $digits[$pos++];
                switch ($country[1]) {
                    case '0':
                    case '7':
                        break; // Those were the only countries with 2 digits in '2x' series
                    case '2':
                    case '3':
                    case '4':
                    case '6':
                        $country .= $digits[$pos++];
                        break; // The many variations for 3-digits countries in '2xx' series
                    default:
                        // Special cases handling in '2xx' series (unassigned, etc)
                        $country .= $digits[$pos++];
                        if (in_array($country, ['210', '214', '215', '217', '219', '259', '292', '293', '294', '295', '296'])) {
                            return static::failUnassigned($value, $country, $context, $constraint);
                        }
                }
                break;
            case '3':
                $country .= $digits[$pos++];
                switch ($country[1]) {
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '6':
                    case '9':
                        break; // Those were the only countries with 2 digits in '3x' series
                    case '5':
                    case '7':
                        $country .= $digits[$pos++];
                        break; // The many variations for 3-digits countries in '3xx' series
                    default:
                        // Special cases handling in '3xx' series (unassigned, etc)
                        $country .= $digits[$pos++];
                        if (in_array($country, ['384', '388'])) {
                            return static::failUnassigned($value, $country, $context, $constraint);
                        }
                }
                break;
            case '4':
                $country .= $digits[$pos++];
                if ($country[1] === '2') {
                    // Special cases handling in '42x' series (unassigned, etc)
                    $country .= $digits[$pos++];
                    if (in_array($country, ['422', '424', '425', '426', '427', '428', '429'])) {
                        return static::failUnassigned($value, $country, $context, $constraint);
                    }
                } // Everything else is a valid 2-digits country in '4x' series
                break;
            case '5':
                $country .= $digits[$pos++];
                if ($country[1] === '0' || $country[1] === '9') {
                    // Everything in '50x' and '59x' series is a valid 3-digits country
                    $country .= $digits[$pos++];
                } // Everything else is a valid 2-digits country in '5x' series
                break;
            case '6':
                $country .= $digits[$pos++];
                if ($country[1] > 6) {
                    // Special cases handling in '6xx' series (unassigned, etc)
                    $country .= $digits[$pos++];
                    if (in_array($country, ['671', '684', '693', '694', '695', '697', '698', '699'])) {
                        return static::failUnassigned($value, $country, $context, $constraint);
                    }
                } // Everything else is a valid 2-digits country in '6x' series
                break;
            case '7':
                // Russia and other territories (1-digit)
                break;
            case '8':
                $country .= $digits[$pos++];
                switch ($country[1]) {
                    case '1':
                    case '2':
                    case '4':
                    case '6':
                        break; // Those were the only countries with 2 digits in '8x' series
                    default:
                        // Special cases handling in '8xx' series (unassigned, etc)
                        $country .= $digits[$pos++];
                        if (in_array($country, ['801', '802', '803', '804', '805', '806', '807', '809', '851', '854', '857', '858', '859', '871', '872', '873', '874', '875', '876', '877', '879', '884', '885', '887', '889'])) {
                            return static::failUnassigned($value, $country, $context, $constraint);
                        }
                }
                break;
            case '9':
                $country .= $digits[$pos++];
                switch ($country[1]) {
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '8':
                        break; // Those were the only countries with 2 digits in '9x' series
                    default:
                        // Special cases handling in '9xx' series (unassigned, etc)
                        $country .= $digits[$pos++];
                        if (in_array($country, ['969', '978', '990', '999'])) {
                            return static::failUnassigned($value, $country, $context, $constraint);
                        }
                }
                break;
            default:
                // '0' is the only left, and is not a valid country code
                return static::failUnassigned($value, $country, $context, $constraint);
        }

        // At this point, $pos indicates the first number not pertaining to the country code.
        $number = substr($digits, $pos);
        return [$country, $number];
    }


    /**
     * Normalize a string as a phone number
     * @param string|null $value
     * @param string $strategy
     * @param string|null $country
     * @param string|null $area
     * @return ?string
     */
    public static function normalize(?string $value, string $strategy=self::NORM_INTERNATIONAL, ?string $country = null, ?string $area = null): ?string
    {
        if (empty($value)) {
            return null;
        }
        if ($strategy === self::NORM_NONE) {
            return $value;
        }
        if (str_starts_with(trim($value), '+')) {
            list($country, $digits) = static::splitCountry($value, null, null);
        }
        else {
            $digits = preg_replace('/[^0-9]/', '', $value);
        }
        if ($strategy === self::NORM_INTERNATIONAL_DIGITS) {
            return $country ? "+{$country}{$digits}" : $digits;
        }
        if ($strategy === self::NORM_LOCAL_DIGITS) {
            return $digits;
        }
        $pretty = static::normalizeLocal($country, $area, $digits);
        if ($strategy === self::NORM_INTERNATIONAL) {
            return $country ? "+{$country} {$pretty}" : $pretty;
        }
        if ($strategy === self::NORM_LOCAL) {
            return $pretty;
        }
        throw new LogicException("Invalid strategy: $strategy");
    }

    /**
     * Normalizes a local number according to its nature
     * @param string|null $country
     * @param string|null $area
     * @param string $digits
     * @return string
     */
    protected static function normalizeLocal(?string $country, ?string $area, string $digits): string
    {
        // Actually, know nothing about it...
        return $digits;
    }
}
