<?php

namespace Saldanhakun\BrazilianValidators\Validator;

use Saldanhakun\BrazilianValidators\Constraint\AbstractPhone;
use Saldanhakun\BrazilianValidators\Constraint\Toll;

/**
 *
 * @author Marcelo Saldanha <saldanha@nautilos.com.br>
 * @license GPL-3.0-or-later
 */
class TollValidator extends PhoneValidator
{

    /**
     * @param string $value
     * @param string $digits
     * @param Toll $constraint
     */
    protected function validateTollNumber(string $value, string $digits, AbstractPhone $constraint): void
    {
        if (preg_match('/^(0300|0500|0800|0900)/', $digits)) {
            if (strlen($digits) === 11) {
                return; // Valid
            }
        }
        // Everything else is rubbish
        $this->context->buildViolation($constraint->message)
            ->setParameter('{{ value }}', $value)
            ->addViolation();
    }

    protected function isLandlineAllowed(): bool
    {
        return false;
    }

    protected function isMobileAllowed(): bool
    {
        return false;
    }

    /**
     * Normalizes a local number according to its nature
     * @param string|null $country
     * @param string|null $area
     * @param string $digits
     * @return string
     */
    protected static function normalizeLocal(?string $country, ?string $area, string $digits): string
    {
        $length = strlen($digits);
        if ($length > 7) {
            return sprintf('%s %s %s', substr($digits, 0, 4), substr($digits, 4, 3), substr($digits, 7));
        }
        return parent::normalizeLocal($country, $area, $digits);
    }
}
