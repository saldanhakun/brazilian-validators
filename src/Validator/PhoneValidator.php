<?php

namespace Saldanhakun\BrazilianValidators\Validator;

use Saldanhakun\BrazilianValidators\Constraint\AbstractPhone;
use Saldanhakun\BrazilianValidators\Constraint\Phone;

/**
 *
 * @author Marcelo Saldanha <saldanha@nautilos.com.br>
 * @license GPL-3.0-or-later
 */
class PhoneValidator extends AbstractPhoneValidator
{

    /**
     * DDD is the brazilian name for local area codes. They are 2-digits, but some values are currently not assigned.
     */
    public const INVALID_DDD = [
        '00', '01', '02', '03', '04', '05', '06', '07', '08', '09',
        '10', '20', '30', '40', '50', '60', '70', '80', '90',
        '23', '25', '26', '29', '39', '52', '56', '57', '58', '59', '72', '76', '78',
    ];

    /**
     *
     * @param string $value
     * @param string $country
     * @param string $digits
     * @param Phone $constraint
     */
    protected function validateInternationalPhone(string $value, string $country, string $digits, AbstractPhone $constraint): void
    {
        if ($country !== '55') {
            if ($constraint->allow_foreign) {
                // This validator knowns nothing about foreign numbers, beyond identifying the country.
                // So, just accept the value as is.
                return;
            }
            // Does not accept foreign numbers
            $this->context->buildViolation($constraint->foreign_message)
                ->setParameter('{{ value }}', $value)
                ->setParameter('{{ country }}', $country)
                ->addViolation();
            return;
        } else {
            $this->validateLocalPhone($value, $digits, $constraint);
        }
    }

    /**
     * @param string $value
     * @param string $digits
     * @param Phone $constraint
     */
    protected function validateLocalPhone(string $value, string $digits, AbstractPhone $constraint): void
    {
        // Tries to identify the number format for the brazilian phone system. There are a few options.
        $length = strlen($digits);
        if (in_array($length, [3, 4, 5])) {
            // Special service numbers, like police, fireman, phone companies, messaging centers, etc
            $this->validateServiceNumber($value, $digits, $constraint);
            return;
        }
        if ($length === 8 || ($length === 9 && $digits[0] === '9')) {
            // There is a kind of 8-digit phone that don't have a local area. These have known prefixes
            // and if these don't match, then we have a common landline phone missing local area code.
            if (preg_match('/^(300|400)/', $digits)) {
                // Makes a distinction between these numbers and common landlines
                $this->validateServiceNumber($value, $digits, $constraint);
                return;
            }
            // The validator can be configured to assume a local area and permit an easier (albeit more
            // dangerous) way for phone inputting.
            if (empty($constraint->assume_local_area)) {
                $this->context->buildViolation($constraint->area_message)
                    ->setParameter('{{ value }}', $value)
                    ->setParameter('{{ code }}', '')
                    ->addViolation();
            }
            // Here we have a landline (8-digit) or mobile (9-digit with leading 9) without local area code.
            // Notice: the validation does not "fix" the phone. Normalization must to this, if needed
            if ($length === 9) {
                $this->validateMobileNumber($value, $constraint->assume_local_area, $digits, $constraint);
            } else {
                $this->validateLandlineNumber($value, $constraint->assume_local_area, $digits, $constraint);
            }
            return;
        }
        if ($length === 10 || ($length === 11 && $digits[2] === '9')) {
            $local_area = substr($digits, 0, 2);
            $number = substr($digits, 2);
            if (in_array($local_area, self::INVALID_DDD, true)) {
                $this->context->buildViolation($constraint->area_message)
                    ->setParameter('{{ value }}', $value)
                    ->setParameter('{{ code }}', $local_area)
                    ->addViolation();
            }
            // Here we have a landline (8-digit) or mobile (9-digit with leading 9) with local area code.
            if ($length === 11) {
                $this->validateMobileNumber($value, $local_area, $number, $constraint);
            } else {
                $this->validateLandlineNumber($value, $local_area, $number, $constraint);
            }
            return;
        }
        if (preg_match('/^(0300|0500|0800|0900)/', $digits)) {
            // These are toll numbers
            $this->validateTollNumber($value, $digits, $constraint);
        }
    }

    // Internal toggle for validation or refusal
    protected function isMobileAllowed(): bool
    {
        return true;
    }
    // Internal toggle for validation or refusal
    protected function isLandlineAllowed(): bool
    {
        return true;
    }

    /**
     * @param string $value
     * @param string $ddd
     * @param string $number
     * @param Phone $constraint
     */
    protected function validateMobileNumber(string $value, string $ddd, string $number, AbstractPhone $constraint): void
    {
        if (!$this->isMobileAllowed()) {
            parent::validateMobileNumber($value, $ddd, $number, $constraint);
            return;
        }

        // A few years ago, starting in 2012, brazilian mobile numbers started migrating from 8-digits (leading
        // with 7, 8 or 9). This process took several years, during which a validation routine would need to
        // consider the area code to handle this ninth digit. As of 2018, all brazilian states completed this
        // migration, so this package no long need to check this issue.
        // Basically, all mobiles have 9 digits, and all of them start with 9.

        // Currently, there are no mobile carriers assigned outside the prefixes 99, 98, 97 and 96.
        // So we could check that.
        if (!in_array($number[1], ['9', '8', '7', '6'], true)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $value)
                ->addViolation();
        }
    }


    /**
     * @param string $value
     * @param string|null $ddd
     * @param string $number
     * @param Phone $constraint
     */
    protected function validateLandlineNumber(string $value, ?string $ddd, string $number, AbstractPhone $constraint): void
    {
        if (!$this->isLandlineAllowed()) {
            parent::validateLandlineNumber($value, $ddd, $number, $constraint);
            return;
        }

        // Currently, only these prefixes (first digit) are allowed in numbers
        if (!in_array($number[0], ['2', '3', '4', '5'], true)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $value)
                ->addViolation();
        }
    }

    /**
     * Normalizes a local number according to its nature
     * @param string|null $country
     * @param string|null $area
     * @param string $digits
     * @return string
     */
    protected static function normalizeLocal(?string $country, ?string $area, string $digits): string
    {
        dump(['country' => $country, 'area' => $area, 'digits' => $digits]);
        if (empty($country) || $country === '55') {
            $length = strlen($digits);
            if ($length === 11 && $digits[2] === '9') {
                return sprintf('(%s) %s-%s',
                    substr($digits, 0, 2),
                    substr($digits, 2, 5),
                    substr($digits, 7)
                );
            }
            if ($length === 10) {
                return sprintf('(%s) %s-%s',
                    substr($digits, 0, 2),
                    substr($digits, 2, 4),
                    substr($digits, 6)
                );
            }
            if ($length === 9 && $digits[0] === '9') {
                if ($area) {
                    return sprintf('(%s) %s-%s',
                        $area,
                        substr($digits, 0, 5),
                        substr($digits, 5)
                    );
                }
                else {
                    return sprintf('%s-%s',
                        substr($digits, 0, 5),
                        substr($digits, 5)
                    );
                }
            }
            if ($length === 8) {
                if ($area) {
                    return sprintf('(%s) %s-%s',
                        $area,
                        substr($digits, 0, 4),
                        substr($digits, 4)
                    );
                }
                else {
                    return sprintf('%s-%s',
                        substr($digits, 0, 4),
                        substr($digits, 4)
                    );
                }
            }
        }
        return parent::normalizeLocal($country, $area, $digits);
    }
}
