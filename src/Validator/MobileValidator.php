<?php

namespace Saldanhakun\BrazilianValidators\Validator;

/**
 *
 * @author Marcelo Saldanha <saldanha@nautilos.com.br>
 * @license GPL-3.0-or-later
 */
class MobileValidator extends PhoneValidator
{

    protected function isLandlineAllowed(): bool
    {
        return false;
    }
}
