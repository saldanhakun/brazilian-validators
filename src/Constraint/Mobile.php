<?php

namespace Saldanhakun\BrazilianValidators\Constraint;

use Saldanhakun\BrazilianValidators\Validator\MobileValidator;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Marcelo Saldanha <saldanha@nautilos.com.br>
 * @license GPL-3.0-or-later
 */
class Mobile extends Phone
{

    /**
     * @inheritdoc
     */
    public function validatedBy(): string
    {
        return MobileValidator::class;
    }
}
