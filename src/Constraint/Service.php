<?php

namespace Saldanhakun\BrazilianValidators\Constraint;

use Saldanhakun\BrazilianValidators\Validator\ServiceValidator;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Marcelo Saldanha <saldanha@nautilos.com.br>
 * @license GPL-3.0-or-later
 */
class Service extends Phone
{

    /**
     * @inheritdoc
     */
    public function validatedBy(): string
    {
        return ServiceValidator::class;
    }
}
