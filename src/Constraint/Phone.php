<?php

namespace Saldanhakun\BrazilianValidators\Constraint;

use Saldanhakun\BrazilianValidators\Validator\PhoneValidator;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Marcelo Saldanha <saldanha@nautilos.com.br>
 * @license GPL-3.0-or-later
 */
class Phone extends AbstractPhone
{

    /**
     * @var string Foreign phone number error
     */
    public $foreign_message = 'The phone "{{ value }}" is not brazilian (country code "{{ country }}").';

    /**
     * @var bool If foreign numbers are allowed. The validotor only goes so far as to detect them, and accept whatever they are.
     */
    public $allow_foreign = false;

    /**
     * @var string|null If the validator should permit phones without local area
     */
    public $assume_local_area = null;

    /**
     * @inheritdoc
     */
    public function validatedBy(): string
    {
        return PhoneValidator::class;
    }
}
