<?php

namespace Saldanhakun\BrazilianValidators\Constraint;

use Saldanhakun\BrazilianValidators\Validator\TollValidator;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Marcelo Saldanha <saldanha@nautilos.com.br>
 * @license GPL-3.0-or-later
 */
class Toll extends Phone
{

    /**
     * @inheritdoc
     */
    public function validatedBy(): string
    {
        return TollValidator::class;
    }
}
