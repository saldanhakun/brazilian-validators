<?php

namespace Saldanhakun\BrazilianValidators\Constraint;

use Saldanhakun\BrazilianValidators\Validator\CpfValidator;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Marcelo Saldanha <saldanha@nautilos.com.br>
 * @license GPL-3.0-or-later
 */
class Cpf extends Constraint
{
    /**
     * @var string General validation error (syntactic)
     */
    public $message = 'The value "{{ value }}" is not a valid CPF.';

    /**
     * @var string Input does not have enough digits to be a valid CPF
     */
    public $length_message = 'The value "{{ value }}" does not have the expected length for a CPF.';

    /**
     * @var string Input verification code does not match what was expected (semantic error)
     */
    public $dv_message = 'The CPF "{{ value }}" fails the validation.';

    /**
     * @var string Input verification code does not match what was expected (semantic error)
     * Includes the expected DV as a hint.
     */
    public $dv_message_hinted = 'The CPF "{{ value }}" fails the validation. Expected "{{ dv }}", got "{{ input_dv }}".';

    /**
     * @var bool If input without leading zeroes is allowed and normalized. Usually safe.
     */
    public $pad_left = true;

    /**
     * @var string If the correct DV should be hinted in the message. Usually only available in DEV environments
     * Valid options are 'no', 'yes' or the key to some environment (e.g. 'dev', 'test', 'prod')
     */
    public $hint_dv = 'dev';

    /**
     * @inheritdoc
     */
    public function validatedBy(): string
    {
        return CpfValidator::class;
    }

}
