<?php

namespace Saldanhakun\BrazilianValidators\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 *
 * @author Marcelo Saldanha <saldanha@nautilos.com.br>
 * @license GPL-3.0-or-later
 */
abstract class AbstractPhone extends Constraint
{
    /**
     * @var string General validation error (syntactic)
     */
    public $message = 'The value "{{ value }}" is not a valid phone.';

    /**
     * @var string Invalid characters found error (syntactic)
     */
    public $invalid_char_message = 'The value "{{ value }}" has invalid data. Use only digits and punctuation.';

    /**
     * @var string Invalid use of plus sign error (semantic)
     */
    public $plus_message = 'The value "{{ value }}" doesn\'t use the "+" signal as expected.';

    /**
     * @var string Usage of unassigned country code (may require future updated if and when the country code list gets updated)
     */
    public $unassigned_country_message = 'The country code "+{{ country }}" in "{{ value }}" is not officially assigned.';

    /**
     * @var string Service number not allowed
     */
    public $service_number_message = 'The phone "{{ value }}" is a service number and is not allowed.';

    /**
     * @var string Toll number not allowed
     */
    public $toll_number_message = 'The phone "{{ value }}" is a toll number and is not allowed.';

    /**
     * @var string Mobile number not allowed
     */
    public $mobile_number_message = 'The phone "{{ value }}" is a mobile number and is not allowed.';

    /**
     * @var string Landline number not allowed
     */
    public $landline_number_message = 'The phone "{{ value }}" is a landline number and is not allowed.';

    /**
     * @var string The value does not have the required length to be valid
     */
    public $length_message = 'The phone "{{ value }}" does not have a valid length.';

    /**
     * @var string The local area code is not valid
     */
    public $area_message = 'The phone "{{ value }}" does not have a valid area code: "{{ code }}".';
}
